Etapes d'installation en vue de la formation "Initiation à python"
=================================

Pour la formation il est nécessaire d'installer:
* Anaconda: distribution ```python```
* Git.exe
* S'inscrire sur GitLab.com pour récupérer le support de formation (optionnel)

Pour installer Anaconda il faut suivre ce tutoriel: 
[installation Anaconda](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_PYTHON.md)

Pour installer git.exe il faut suivre ce tutoriel: 
[installation git](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_GIT_EXE.md)

Pour s'inscrire sur GitLab.com: [inscription à 
gitlab](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSCRIPTION_GITLAB.md)

L'ensemble des packages nécessaires à la formation sont normalement déjà présents dans la distribution 
Anaconda.
